package mx.sidlors.quiz.model;


/**
 * Enum for Genre support
 */
public enum Genre{

    //Here we can add/remove Genre
    ROCK(0),COUNTRY(1), RAGGE(2), SALSA(3), DANCE(4),POP(5);
    private final int genre;
    
    private Genre(int genre){
        this.genre = genre;
    }    

    //Transfor numId for Genre
    public static Genre findById(int id){
        for(Genre g : values()){
            if( g.getGenreValue()==id ){
                return g;
            }
        }
        return null;
    }

    //method for max value for range of ramdom funtion in RandomStuff
    public static int genreMaxValue(){
        int i=0;
        for (Genre g : values()){ 
            i++;
        }
        return i;
    }

    public int getGenreValue(){
        return this.genre;
    }
} 