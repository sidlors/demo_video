package mx.sidlors.quiz.model;
import java.math.BigInteger;

/*
* class for buisness Video model
* likes:BigInteger for likes incremental value
* Genre:  genere enum  instance 
*/
public class Video implements Comparable<Video>{

private BigInteger likes;
private String genre;


public Video(BigInteger likes,String genre){
    this.likes=likes;
    this.genre=genre;
}

public void setGenre(String genre){

    this.genre=genre;

}



//here we need to protect writting 
public synchronized void  setLikes(BigInteger likes){

    this.likes=likes;
}

public BigInteger getLikes(){

    return likes;

}

//Here we don't need to block 
public String getGenre(){

    return genre;

}

    /*
        Compare Video likes
    */
    @Override
    public int compareTo(Video o) {
        return this.likes.compareTo(o.getLikes());
    }


}
