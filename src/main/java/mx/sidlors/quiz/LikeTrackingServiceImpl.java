package mx.sidlors.quiz;
import java.math.BigInteger;
import java.util.Map;
import java.util.Map.Entry;
import java.util.List;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.stream.Collectors;

import mx.sidlors.quiz.model.Video;

public class LikeTrackingServiceImpl implements  LikeTrackingService {

    private final Map<String,Video> likeTracks;


	public LikeTrackingServiceImpl(){
		likeTracks =new HashMap<String,Video>();
		

	} 

    // for write new video or update O(n)=n in complexity time
	public void recordLikes(String videoId, BigInteger currentLikes, String genre){

        Video currentVideo=new Video(currentLikes,genre);
        likeTracks.put(videoId,currentVideo);

    } 

    // we here just to be java8 stream api, for sorting values & recollect result in
    // list, time xomplexity O(n)= nlog n   acording to java collection api
        public Map<String,Video> getTopLiked(){

        final Map<String, Video> sortedByCount = likeTracks.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));


        return sortedByCount;
    }
    
	
    // we here just to be java8 stream api, for sorting values & recollect result in
    // list, time xomplexity O(n)= nlog n   acording to java collection api
    public BigInteger getNumberOfLikesForGenre(String genre){
        
        BigInteger likes=new BigInteger("0");
        
       likeTracks.values()
                .stream()
                .filter(x->x.getGenre().equals(genre))
                .collect(Collectors.toList());
                                    
        return likes;
    }


}
