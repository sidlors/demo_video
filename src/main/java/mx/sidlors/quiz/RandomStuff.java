package mx.sidlors.quiz;

import java.util.Random;
import mx.sidlors.quiz.model.Genre;


/*
* Ramdon utilities for create id, bigInteger, and Ramdom
* Genere enum.
*/
public class RandomStuff { 

	// function to generate a random string of length n 
	public static String getAlphaNumericString(int n) { 
		// chose a Character random from this String 
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
									+ "0123456789"
									+ "abcdefghijklmnopqrstuvxyz"; 
		// create StringBuffer size of AlphaNumericString 
		StringBuilder sb = new StringBuilder(n); 
		for (int i = 0; i < n; i++) { 
			// generate a random number between 
			// 0 to AlphaNumericString variable length 
			int index 
				= (int)(AlphaNumericString.length() 
						* Math.random()); 
			// add Character one by one in end of sb 
			sb.append(AlphaNumericString 
						.charAt(index)); 
		} 
		return sb.toString(); 
	} 


		// function to generate a random string of length n 
	public static int getNumeric( ) { 
			Random r = new Random();
				int index = Math.abs(r.nextInt());
			return index; 
	}

	//Randorm Genre 
	public static Genre getRandomGenre() {
		return Genre.findById(generateRandomIntIntRange(0, Genre.genreMaxValue()-1));
	}

	//Ramdon Intger between min and max parameters
	private static int generateRandomIntIntRange(int min, int max) {
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
} 
