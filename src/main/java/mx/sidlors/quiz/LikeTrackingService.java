package mx.sidlors.quiz;

import java.math.BigInteger;
import java.util.Map;

import mx.sidlors.quiz.model.Video;

public interface LikeTrackingService {
	void recordLikes(String videoId, BigInteger currentLikes, String genre); 

	public Map<String,Video> getTopLiked();
	
	BigInteger getNumberOfLikesForGenre(String genre);
}
