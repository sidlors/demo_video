package mx.sidlors.quiz;
import java.math.BigInteger;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.sidlors.quiz.model.Video;

public class MainThread implements Runnable{

 private static final Logger LOGGER = LogManager.getLogger(MainThread.class.getName());	

 private static LikeTrackingService LikeTrackingService=new LikeTrackingServiceImpl();




    @Override
    public void run(){
		LOGGER.info("Thread ID-"  + Thread.currentThread().getId() +
						  " For " + Thread.currentThread().getName());
		try{
            Thread.sleep(100);
            LikeTrackingService.recordLikes(RandomStuff.getAlphaNumericString(10),
                                             new BigInteger(""+RandomStuff.getNumeric()),
                                             RandomStuff.getRandomGenre().toString());
        }catch (InterruptedException e) {                
			LOGGER.error("we have an exception: "+ e.getMessage());
        }
    }

    public static void main(String[] args){

        Runtime runtime= Runtime.getRuntime();
        int nNucleos=runtime.availableProcessors();
        LOGGER.info("We have "+ nNucleos +" cores detected ");
        // # threads accourding cores
        Thread [] threads=new Thread[nNucleos*10];

        for(int i=0;i<threads.length;i++){
            Runnable runnable=new MainThread();
            threads[i]=new Thread(runnable,"thread-"+i);
            threads[i].start();
        }

        // we  join threads in a barrier
        for(int i=0;i<threads.length;i++){
            try{
                threads[i].join();
            }catch(Exception ex){
                LOGGER.error("we have an exception: "+ ex.getMessage());
            }

        }

        verificaData();
       

    }

    private static void verificaData() {

        Map<String,Video> result=LikeTrackingService.getTopLiked();
        result.forEach((k,v)->LOGGER.info("Id : " + k 
                                            + " Genre: " + v.getGenre()
                                            + " Likes: "+ v.getLikes() ));
        String randomGenre=RandomStuff.getRandomGenre().toString();
        BigInteger likes=LikeTrackingService.getNumberOfLikesForGenre(randomGenre);

        LOGGER.info("Genre: " + randomGenre+" likes "+likes);
        /*
        Video video=result.get("21312331");
        LOGGER.info("message "+video.getGenre() +" likes: " + video.getLikes() );
        */
    }
}
